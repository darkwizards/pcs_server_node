"use strict";
/**
 * Created by BANO.notIT on 06.12.16.
 */
import express from "express";
import bodyParser from "body-parser";
import {UNKNOWN} from "./modules/Errors";
import Users from "./routes/Users";
import Meets from "./routes/Meets";
import Streams from "./routes/Streams";
import options from "./conf/server.json";
import {join} from "path";
import {version} from "./package.json";
import Debug from "debug";


const app = express(),

    Glob = Debug('app:global'),
    Request = Debug('app:request'),
    Error = Debug('app:error');

Glob("Started at", new Date());

// Убираем наляпку, чтобы никто не догадался, что мы на экспрессе :D
app.disable('x-powered-by');


app
    .use(bodyParser.json())
    .use(bodyParser.urlencoded({extended: true}));


// Логирование всех запросов
app.use(function (req, res, next) {

    // Добавляем свою наляпку, чтобы в случае чего можно было сказать, что мы хорошие и сделали всё сами)
    res.header("X-Powered-By", `Node.js (${process.versions.node}); Aquarius (${version})`);

    if (process.argv[2] !== "silent") {
        Request('%O', {
            url: req.method + ":" + req.url,
            body: req.body,
            query: req.query,
            headers: req.headers
        });
    }
    next();

});


app
    .use(Users)
    .use(Meets)
    .use(Streams)
    .use(function (err, req, res, next_err) {

        Error(err);

        if ("errorText" in err)
            res.status(400).send(err);

        else
            res.status(500).send(UNKNOWN)

    })
;


// Ну и наконец слушаем
app.listen(options.port, () => Glob("Listening on port " + options.port));