"use strict";
/**
 * Created by BANO.notIT on 06.12.16.
 * @namespace db
 */
import mongoose from "mongoose";
import conf from "../conf/server.json";
import crypto from "crypto";
import {Schema} from "mongoose/lib";

mongoose.Promise = Promise;
mongoose.connect(conf.db);

const
    {ObjectId:Id} = Schema;

export const
    rmongoid = /^[a-f\d]{24}$/i,
    rmongoids = /^[a-f\d]{24}(,[a-f\d]{24})*$/i,
    rmd5 = /^.{32}$/,
    rclasses = /.+/i,
    remail = /@.+(\..+)+$/i;


let user = Schema({
    children: [String],
    name: String,
    type: {
        type: Number,
        default: 1
    },
    status: {
        type: String,
        default: ""
    },
    mail: {
        type: String,
        validate: remail,
        required: !0,
        unique: !0
    },

    passHash: {
        type: String
    },

    passS: {
        type: String,
        default: Math.random() + ""
    }
});
user.virtual("pass").set(function (data) {
    this.passHash = crypto.createHash("sha256").update(data + this.passS).digest("hex");
});
user.methods = {
    isPass(pass) {
        return this.passHash == crypto.createHash("sha256").update(pass + this.passS).digest("hex");
    }
};
export const User = mongoose.model("User", user);

export const MEET_NOT_STARTED_TIME = 0;
export const MEET_NOT_ENDED_TIME = 0;
let meeting = Schema({
    recipient_id: Id,
    owner_id: Id,
    stream_id: Id,
    time_start: Number,
    duration: Number,
    started: {
        type: Number,
        default: MEET_NOT_STARTED_TIME
    },
    ended: {
        type: Number,
        default: MEET_NOT_ENDED_TIME
    }
}, {toJSON: {virtuals: true}});
export const Meet = mongoose.model("Meeting", meeting);


let stream = Schema({
    owner_id: Id,
    time_start: Number,
    time_end: Number,
    repeat: {
        type: Boolean,
        default: false
    },
    description: String,
    duration: {
        type: Number,
        default: 5
    },
    status: Number
}, {toJSON: {virtuals: true}});
export const Stream = mongoose.model("Stream", stream);


let token = Schema({
    user_id: Id,
    expire: {
        type: Date,
        expire: 108e5,
        default: Date.now
    }
}, {toJSON: {virtuals: true}});
token.methods = {
    getUser(){
        return User.findById(this.user_id).exec()

    }
};
export const TokenPair = mongoose.model("TokenPair", token);


let verPair = Schema({
    data: {
        type: Object,
        unique: true
    },
    expire: {
        type: Date,
        expire: 108e5,
        default: Date.now
    }
}, {toJSON: {virtuals: true}});
export const VerifycationPair = mongoose.model("VerificationPair", verPair);

