"use strict";
/**
 * Created by BANO.notIT on 27.12.16.
 */
import {Router} from "express";
import {authMiddleware} from "./Users";
import {Meet, Stream, User, MEET_NOT_STARTED_TIME} from "../models";
import Interval from "../modules/Interval";
import IdConv from "../modules/_id2id";
import NotEmpty from "../modules/IfNotEmpty";
import OmitSecs from "../modules/SimphyMins";
import ifDataMatches from "../modules/Validator";
import queue from "async/queue";
import {MEET_NOT_STARTED} from "../modules/Errors";
import Debug from "debug";

const
    QueueCreator_log = Debug("app:queue:creator"),
    UserRemover_log = Debug("app:queue:user_remover");


let
    CreatingQueue = queue(function ({req, res, next}, callback) {

        let {stream_id, min_time, owner_id, duration} = req.data;

        min_time = OmitSecs(+min_time);

        const
            MINUTE = 6e4,
            WEEK = 10080 * MINUTE;

        Stream.findById(stream_id, {
            _id: 1,
            time_start: 1,
            time_end: 1,
            repeat: 1,
            owner_id: 1,
            duration: 1
        }).exec()
            .then(function (stream) {

                let
                    recipient_id = stream.owner_id
                    ;

                owner_id = owner_id && req.user.type == 0 ? owner_id : req.user._id;

                return Promise.all([

                    Meet.find(
                        {
                            $or: [
                                {owner_id: {$in: [owner_id, recipient_id]}},
                                {recipient_id: {$in: [owner_id, recipient_id]}}
                            ]
                        },
                        {
                            time_start: 1,
                            duration: 1
                        }
                    ).sort({time_start: 1}).exec(),

                    stream

                ])
            })
            .then(function ([meetings, stream]) {

                let
                    interval = new Interval(-Infinity, stream.time_start),
                    duration = duration !== undefined && req.user.type != 1 ? duration : stream.duration
                    ;

                // Ограничители
                interval.join(-Infinity, OmitSecs(+new Date));
                interval.join(-Infinity, min_time);

                const clearance = MINUTE * 5; // five minutes

                console.log(meetings);

                meetings.forEach(meet =>
                    interval.join(
                        OmitSecs(meet.time_start),
                        OmitSecs(
                            meet.time_start +
                            meet.duration * MINUTE +
                            (
                                meet.owner_id == owner_id ?
                                    clearance :
                                    0
                            )
                        )
                    )
                );

                if (stream.repeat) {
                    let
                        pieces = interval.pieces,
                        between = pieces[pieces.length - 1].end - stream.time_start;

                    for (let i = 0, a = Math.min(between / WEEK, 1) + 1; i < a; i++)
                        interval.join(
                            stream.time_end + WEEK * i,
                            stream.time_start + WEEK * (i + 1)
                        )

                } else
                    interval.join(stream.time_end, Infinity);

                let
                    holes = interval.holes.pieces,
                    cur = null;

                holes.some(piece =>
                    piece.length >= duration * MINUTE ? ((cur = piece), true) : false
                );

                if (cur == null) {
                    return Promise.reject(MEET_NOT_STARTED)
                }

                let meet = new Meet({
                    recipient_id: stream.owner_id,
                    owner_id,
                    stream_id,
                    time_start: cur.start,
                    duration
                });

                return meet.save()

            })
            .then(function (meet) {

                res.json(IdConv(meet.toObject()));
                callback();

            })
            .catch(function (err) {

                next(err);
                QueueCreator_log(err);
                callback();

            });

    }),
    RemoveTempUser = queue(function (usrIds, callback) {

        User.findOne({
            status: "temporary",
            type: 1,
            _id: {$in: usrIds}
        }).exec()
            .then(function (usr) {

                if (usr !== null)
                    return Promise.all([

                        Meet.find({
                            $or: [{
                                recipient_id: {$in: usrIds}
                            }, {
                                owner_id: {$in: usrIds}
                            }]
                        }).exec(),

                        usr

                    ])

            })
            .then(function ([meets, usr]) {

                // если такой особенный юзверь больше не имеет митингов, то мы его удаляем
                if (meets.length === 0)
                    usr.remove();

                callback();

            })
            .catch(function (error) {

                UserRemover_log(error);
                callback();

            })

    });


export default Router()
    .get("/getAllMeetings", authMiddleware, function (req, res, next) {

        let search =
            req.user.type > 0 ?
                {$or: [{owner_id: req.user.id}, {recipient_id: req.user.id}]} :
                {};

        Meet.find(search).exec()
            .then(function (meets) {

                res.json(Array.from(meets).map(meet => IdConv(meet.toObject())))

            })
            .catch(function (error) {

                next(error)

            })


    })
    .get("/getMeeting", authMiddleware, ifDataMatches({
        id: /^[a-f\d]{24}$/i
    }), function (req, res, next) {

        let {id} = req.data;

        Meet.findById(id).exec()
            .then(NotEmpty)
            .then(function (meet) {

                res.json(IdConv(meet.toObject()))

            })
            .catch(function (error) {

                next(error)

            })

    })
    .post("/createMeeting", authMiddleware, ifDataMatches({
        stream_id: /^[a-f\d]{24}$/i,
        min_time: /^\d+$/, // Unix Timestamp
        "?owner_id": /^[a-f\d]{24}$/i, // Optional
        "?duration": /^\d+$/ // Optional
    }), function (req, res, next) {

        CreatingQueue.push({req, res, next}, new Function())

    })
    .post("/editMeeting", authMiddleware, ifDataMatches({
        id: /^[a-f\d]{24}$/i,
        recipient_id: /^[a-f\d]{24}$/i,
        owner_id: /^[a-f\d]{24}$/i,
        stream_id: /^[a-f\d]{24}$/i,
        time_start: /^\d+$/,
        duration: /^\d+$/,
        "?started": /^\d+/,
        "?ended": /^\d+/
    }), function (req, res, next) {

        let
            {
                id,
                recipient_id,
                owner_id,
                stream_id,
                time_start,
                started,
                ended,
                duration
            } = req.data,
            search =
                req.user.type > 0 ?
                    {$or: [{owner_id: req.user._id, _id: id}, {recipient_id: req.user._id, _id: id}]} :
                    {_id: id};

        Meet
            .findOneAndUpdate(search, {
                recipient_id,
                owner_id,
                stream_id,
                time_start,
                started,
                ended,
                duration
            }, {new: true}).exec()
            .then(NotEmpty)
            .then(function (meet) {

                res.json(IdConv(meet.toObject()))

            })
            .catch(function (error) {

                next(error);

            })

    })
    .post("/removeMeeting", authMiddleware, ifDataMatches({
        id: /^[a-f\d]{24}$/i
    }), function (req, res, next) {

        let
            {id} = req.data,
            search = req.user.type !== 0 ?
                {
                    $or: [{
                        recipient_id: req.user.id
                        , _id: id
                    }, {
                        owner_id: req.user.id,
                        _id: id
                    }]
                } :
                {_id: id};

        Meet.findOneAndRemove(search, {select: {_id: 1}})
            .then(NotEmpty)
            .then(function () {

                res.json(true)

            })
            .catch(function (error) {

                next(error);

            })

    })
    .post("/startMeeting", authMiddleware, ifDataMatches({id: /^[a-f\d]{24}$/i}), function (req, res, next) {

        let {id} = req.data;

        let query = req.user.type === 0 ?
            {
                _id: id
            } :
            {
                $or: [{
                    recipient_id: req.user.id,
                    _id: id
                }, {
                    owner_id: req.user.id,
                    _id: id
                }]
            };

        (function () {

            if (req.user.type === 0)
                return Promise.resolve();

            else
                return (
                    Meet
                        .update({
                            $or: [{
                                recipient_id: req.user.id,
                                started: {$ne: MEET_NOT_STARTED_TIME}
                            }, {
                                owner_id: req.user.id,
                                started: {$ne: MEET_NOT_STARTED_TIME}
                            }]
                        }, {
                            ended: +new Date()
                        })
                        .exec()
                )

        })()
            .then(function () {

                return Meet.findOneAndUpdate(query, {started: +new Date()}, {new: true}).exec()

            })
            .then(NotEmpty)
            .then(function (meet) {

                res.json(meet);

            })
            .catch(function (error) {

                next(error)

            })

    })
    .post("/endMeeting", authMiddleware, ifDataMatches({id: /^[a-f\d]{24}$/i}), function (req, res, next) {

        // todo реализовать алгоритм отбрасывания в конце очереди, при опоздании.

        let {id} = req.data;

        let query = req.user.type === 0 ?
            {
                _id: id
            } :
            {
                $or: [{
                    recipient_id: req.user.id,
                    _id: id
                }, {
                    owner_id: req.user.id,
                    _id: id
                }]
            };

        Meet.findOneAndUpdate(query, {ended: +new Date()}, {new: true}).exec()
            .then(function (meet) {

                res.json(meet);

                return [meet.recipient_id, meet.owner_id]

            })
            .then(function (usrIds) {

                RemoveTempUser.push(usrIds);

            })
            .catch(function (error) {

                next(error)

            })

    })
;
