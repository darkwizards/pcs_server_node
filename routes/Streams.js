"use strict";
/**
 * Created by BANO.notIT on 27.12.16.
 */
import {Router} from "express";
import {ACCESS_DENIED} from "../modules/Errors";
import {authMiddleware} from "./Users";
import {Stream} from "../models";
import IdConv from "../modules/_id2id";
import NotEmpty from "../modules/IfNotEmpty";
import ifDataMatches from "../modules/Validator";


export default Router()
    .get("/getAllStreams", authMiddleware, function (req, res, next) {

        Stream.find(req.user.type != 2 ? {} : {owner_id: req.user.id}).exec()
            .then(function (streams) {

                res.json(streams.map(stream => IdConv(stream.toObject())));

            })
            .catch(function (error) {

                next(error)

            })

    })
    .get("/getStream", authMiddleware, ifDataMatches({id: /^[a-f\d]{24}$/i}), function (req, res, next) {

        let {id} = req.query;

        Stream.findById(id).exec()
            .then(NotEmpty)
            .then(function (stream) {

                res.json(null !== stream ? IdConv(stream.toObject()) : null)

            })
            .catch(function (error) {

                next(error)

            })

    })
    .post("/createStream", authMiddleware, ifDataMatches({
        desc: String,
        time_start: /^\d+$/,
        "?time_end": /^\d+$/,
        repeat: ["1", "0"],
        duration: /^\d+$/
    }), function (req, res, next) {

        // if admin or teacher
        // shitty, but works faster
        if (req.user.type + (req.user.type ^ 2) == 0)
            return next(ACCESS_DENIED);

        let {
            desc,
            time_start,
            time_end,
            duration,
            repeat
        } = req.body;

        let stream = new Stream({
            owner_id: req.user.id,
            description: desc,
            time_start,
            duration,
            time_end,
            repeat: Boolean(+repeat)
        });

        stream.save()
            .then(function (stream) {

                res.json(IdConv(stream.toObject()));

            })
            .catch(function (error) {

                next(error)

            });

    })
    .post("/removeStream", authMiddleware, ifDataMatches({id: /^[a-f\d]{24}$/i}), function (req, res, next) {

        if (req.user == 1)
            return next(ACCESS_DENIED);

        let {id} = req.body;

        Stream.remove({_id: id})
            .then(NotEmpty)
            .then(function () {

                res.json(true);

            })
            .catch(function (error) {

                next(error)

            })

    })
    .post("/editStream", authMiddleware, ifDataMatches({
        id: /^[a-f\d]{24}$/i,
        owner_id: /^[a-f\d]{24}$/i,
        time_start: /^\d+$/,
        time_end: /^\d+$/,
        description: String,
        repeat: ["1", "0"],
        duration: /^\d+$/
    }), function (req, res, next) {

        if (req.user.type == 1)
            return next(ACCESS_DENIED);

        let
            {
                id,
                owner_id,
                time_start,
                time_end,
                description,
                duration,
                repeat
            } = req.body;

        console.log(Boolean(+repeat));

        Stream
            .findOneAndUpdate(
                {_id: id},
                {
                    owner_id,
                    time_start,
                    time_end,
                    description,
                    duration,
                    repeat: Boolean(+repeat)
                },
                {new: true}
            ).exec()
            .then(NotEmpty)
            .then(function (stream) {

                res.json(IdConv(stream.toObject()))

            })
            .catch(function (error) {

                next(error);

            })

    })