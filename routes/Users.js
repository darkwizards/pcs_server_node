"use strict";
/**
 * Created by BANO.notIT on 27.12.16.
 */
import {Router} from "express";
import {User, TokenPair, VerifycationPair, remail, rclasses, rmd5, rmongoid, rmongoids} from "../models";
import {USR_EXIST, UNAUTH, LOG_PASS, ACCESS_DENIED, NO_USER} from "../modules/Errors";
import IdConv from "../modules/_id2id";
import {send as sendMail} from "../modules/Mail";
import NotEmpty from "../modules/IfNotEmpty";
import ifDataMatches from "../modules/Validator";
import conf from "../conf/server.json";


export function authMiddleware(req, _, next) {

    let {token} = req.method == "POST" ? req.body : req.query;

    if (token && token.length && token.length == 24)
        TokenPair.findById(token).exec()
            .then(NotEmpty)
            .then(function (token) {

                return token.getUser();

            })
            .then(function (user) {

                req.user = user;
                req.data = req.method == "POST" ? req.body : req.query;

                next();

            })
            .catch(function () {

                next(UNAUTH)

            });
    else
        next(UNAUTH)

}


export default Router()
    .post("/register", ifDataMatches({
        status: String,
        name: String,
        children: rclasses,
        mail: remail,
        pass: rmd5,
        type: ["1", "2"]
    }), function (req, res, next) {

        let
            {name, children, mail, pass, type, status} = req.body;

        mail = mail.toLowerCase();

        if (type === 0)
            return next(ACCESS_DENIED);

        User.findOne({mail})
            .then(function (user) {

                if (user !== null)
                    return Promise.reject(USR_EXIST);

                return Promise.resolve()

            })
            .then(function () {

                return VerifycationPair.findOne({data: {mail}}).exec()

            })
            .then(function (verPair) {

                if (verPair !== null)
                    return Promise.reject(USR_EXIST);

                return Promise.resolve()

            })
            .then(function () {

                return VerifycationPair({data: {name, children: children.split(","), status, mail, pass, type}})
                    .save()

            })
            .then(function (pair) {

                sendMail({
                    to: mail,
                    template: 'mailVerify',
                    env: {
                        id: pair._id,
                        current_net_addr: conf.mailVerifyDomain
                    }
                });

                res.json(true);

            })
            .catch(function (error) {

                next(error)

            })

    })
    .post("/login", ifDataMatches({
        mail: remail,
        pass: rmd5
    }), function (req, res, next) {

        let {mail, pass} = req.body;

        User.findOne({mail: mail.toLowerCase()}).exec()
            .then(function (user) {

                if (user === null)
                    return Promise.reject(NO_USER);

                if (user.isPass(pass))
                    return new TokenPair({user_id: user._id})
                        .save();

                else
                    return Promise.reject(LOG_PASS);


            })
            .then(NotEmpty)
            .then(function (token) {

                res.json({token: token._id, id: token.user_id});

            })
            .catch(function (err) {

                next(err)

            })
    })
    .get("/verifyMail:id", function (req, res, _) {

        VerifycationPair.findById(req.params.id).exec()
            .then(NotEmpty)
            .then(function (pair) {

                let data = pair.data;

                return VerifycationPair.remove({_id: pair.id})
                    .then(function () {
                        return (new User(data)).save()
                    })

            })
            .then(function (usr) {

                return new TokenPair({user_id: usr._id}).save();

            })
            .then(function (token) {

                res.cookie('session', token.user_id + '|' + token._id, {encode: String, maxAge: 864e5});

                res.redirect("/")

            })
            .catch(function () {

                res.redirect("/mail_failed")

            })

    })
    .get("/getUser", authMiddleware, ifDataMatches({
        id: rmongoid
    }), function (req, res, next) {

        let {id} = req.query;

        User.findById(id, {name: 1, mail: 1, type: 1, status: 1, children: 1}).exec()
            .then(NotEmpty)
            .then(function (user) {

                res.json(IdConv(user.toObject()));

            })
            .catch(function (error) {

                next(error)

            })

    })
    .get("/getUsers", authMiddleware, ifDataMatches({"?ids": rmongoids}), function (req, res, next) {

        if (req.user.type !== 0)
            return next(ACCESS_DENIED);

        User.find(
            req.data.ids && req.data.ids.length ?
                {_id: {$in: req.data.ids.split(",")}} :
                {}
        ).exec()
            .then(function (users) {

                res.json(users.map(el => el.toObject()).map(IdConv));

            })
            .catch(function (error) {

                next(error);

            })

    })
    .post("/createTemporaryUser", authMiddleware, ifDataMatches({
        name: String
    }), function (req, res, next) {

        let {name} = req.body;

        if (req.user.type !== 0)
            return next(ACCESS_DENIED);

        let usr = new User({
            name,
            type: 1,
            status: "temporary",
            mail: (+new Date()) + "@m.r",
            pass: Math.random() * 0xffff
        });

        usr.save()
            .then(function (usr) {

                res.json(IdConv(usr.toObject()));

            })
            .catch(function (error) {

                next(error);

            })

    })
    .post("/createUser", authMiddleware, ifDataMatches({
        name: String,
        children: rclasses,
        status: String,
        type: "012".split(''),
        pass: String
    }), function (req, res, next) {

        let {
            name,
            type,
            children,
            status,
            mail,
            pass
        } = req.body;

        mail = mail.toLowerCase();

        if (req.user.type !== 0)
            return next(ACCESS_DENIED);

        let usr = new User({
            name,
            type,
            children: children.split(","),
            status,
            mail,
            pass
        });

        usr.save()
            .then(function (usr) {

                res.json(IdConv(usr.toObject()));

            })
            .catch(function (error) {

                next(error);

            })

    })
    .post("/editUser", authMiddleware, ifDataMatches({
        id: rmongoid,
        type: "012".split(''),
        status: String,
        name: String,
        children: rclasses,
        mail: remail,
        "?pass": rmd5
    }), function (req, res, next) {

        // Admin only
        if (req.user.type !== 0)
            return next(ACCESS_DENIED);

        let
            {
                id: _id,
                type,
                status,
                name,
                children,
                mail,
                pass
            } = req.body;

        mail = mail.toLowerCase();

        let fin = {
            type,
            status,
            name,
            children: children.split(","),
            mail
        };

        if (pass)
            fin.pass = pass;

        User.findById(_id).exec()
            .then(NotEmpty)
            .then(function (user) {

                for (let prop in fin)
                    if (fin.hasOwnProperty(prop))
                        user[prop] = fin[prop];

                return user.save()

            })
            .then(function (user) {

                res.json(IdConv(user.toObject()));

            })
            .catch(function (error) {

                next(error)

            })

    })
    .post("/removeUser", authMiddleware, ifDataMatches({id: rmongoid}), function (req, res, next) {

        if (req.user.type !== 0)
            return next(ACCESS_DENIED);

        User.findOneAndRemove({_id: req.data.id})
            .then(NotEmpty)
            .then(function () {

                res.json(true)

            })
            .catch(function (error) {

                next(error);

            })

    })
;
