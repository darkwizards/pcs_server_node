"use strict";
/**
 * Created by BANO.notIT on 27.12.16.
 */
export const UNAUTH = {error: 0, errorText: "Unauthorized"};
export const ACCESS_DENIED = {error: 1, errorText: "Access Denied"};
export const WRONG_FIELD = {error: 2, errorText: "Wrong format of field/fields"};
export const NO_USER = {error: 3, errorText: "No User"};
export const LOG_PASS = {error: 4, errorText: "Wrong pass/log"};
export const MEET_NOT_STARTED = {error: 5, errorText: "Meeting not started"};
export const UNKNOWN = {error: 6, errorText: "Unknown error"};
export const NO_ID = {error: 7, errorText: "No object with that id"};
export const USR_EXIST = {error: 8, errorText: "User with that email exits"};