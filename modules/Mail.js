"use strict";
/**
 * Created by BANO.notIT on 05.01.17.
 */
import nodemailer from "nodemailer";
import {mail_address} from "../conf/server.json";
import queue from "async/queue";
import {readFile} from "fs";
import CSON from "cson";
import Render from "./Renderer";
import path from "path";


const transport = nodemailer.createTransport(mail_address);

let mailQueue = queue(function (obj, callback) {

    transport.sendMail(obj.mail, function (error) {

        if (error && obj.count) {

            console.log("===Mail Error===");
            console.log(error);
            if (obj.count)
                console.log("===Starting Next Send-Round===");
            console.log("==/Mail Error\\==");

            mailQueue.push({
                count: obj.count - 1,
                mail: obj.mail
            })
        }

        callback()

    })
});


/**
 * @param {string} to - Recipient email address
 * @param {string} template - Template name
 * @param {object} env - Values for template
 * @param {number?} count - Number of "send-rounds"
 */
export function send({to, template, env, count = 3}) {

    readFile(path.resolve(__dirname, '..', "templs", template + ".cson"), 'utf8', function (error, res) {

        if (error) {

            console.log('===FS Error===');
            console.log(error);
            console.log('==/FS Error\\==');

            return null
        }

        CSON.parseCSONString(res.toString(), function (error, data) {

            if (error) {

                console.log('===CSON Parser Error===');
                console.log(error);
                console.log('==/CSON Parser Error\\==');

                return null
            }

            mailQueue.push({
                count,
                mail: {
                    from: "VerQ <" + mail_address.match(/[^\/]+%40[^:]+/)[0].replace("%40", "@") + ">",
                    to,
                    subject: Render(data.subject, env),
                    text: Render(data.text, env),
                    html: Render(data.html, env)
                }
            })

        })

    })
}