"use strict";
/**
 * Created by BANO.notIT on 10.01.17.
 */
import schema from "js-schema";
import {WRONG_FIELD} from "./Errors";


export default function (pattern) {

    let sc = schema(pattern);

    return function (req, res, next) {

        if (sc(req.method == "POST" ? req.body : req.query))
            next();

        else
            next(WRONG_FIELD);

    }

}