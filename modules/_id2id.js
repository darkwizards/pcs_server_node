"use strict";

import pick from "lodash/pick";
import without from "lodash/without";


export default function (obj) {

    obj = {...obj, id: obj._id};

    let keys = without(Object.keys(obj), '__v', '_id', 'passHash', 'passS');

    return pick.apply(null, [obj, ...keys]);

}