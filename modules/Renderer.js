"use strict";
/**
 * Created by BANO.notIT on 23.02.17.
 */


/**
 * Name must match [_a-zA-Z]
 * @param {String} str - TempString
 * @param {Object} val
 * @example
 * render("{{val}}",{val:"hello world"}); // => "hello world"
 */
export default function (str, val) {

    return str.toString()
        .replace(/\{\{([_a-z]+)}}/ig, function (_, name) {

            if (val.hasOwnProperty(name))
                return val[name] + "";

            else
                return ""

        })

}