"use strict";
/**
 * Created by BANO.notIT on 17.12.16.
 */

export default class Interval {

    /**
     * @constructor
     * @param {Number|Interval} start
     * @param {Number}[end]
     */
    constructor(start, end) {

        if (end == undefined && start instanceof Interval)
            return start;

        this._intervals = [start, end];

    }

    /**
     * @param {Number|Array.<Number>} start
     * @param {Number} [end]
     * @return {Interval}
     */
    join(start, end) {

        if (Array.isArray(start))
            [start, end] = start;

        let incom = [start, end];


        // обработка частных случаев
        if (start == end)
            return this;

        else if (this._intervals[0] > end)
            this._intervals = [...incom, ...this._intervals];

        else if (this._intervals[0] == end)
            this._intervals[0] = start;

        else if (this._intervals.slice(-1)[0] < start)
            this._intervals = [...this._intervals, ...incom];

        else if (this._intervals.slice(-1)[0] == start)
            this._intervals[this._intervals.length - 1] = end;

        else {

            let
                first_deleted_index = -1,
                last_deleted_index = 0,
                inject = [];

            this._intervals.some(function (el, i) {

                last_deleted_index = i;

                if (el > end)
                    return true; // stopping loop

                first_deleted_index = el < start ? i : first_deleted_index;

            });

            console.log("FDI(" + start + "):", first_deleted_index, "; LDI(" + end + "):", last_deleted_index);

            if (first_deleted_index === -1 || (first_deleted_index & 1))
                inject.push(start);

            let needMoveLast =
                (last_deleted_index & 1) &&
                last_deleted_index + 1 == this._intervals.length &&
                this._intervals[last_deleted_index] < end;

            if (needMoveLast)
                inject.push(end);


            this._intervals = this._intervals
                .slice(0, first_deleted_index + 1)
                .concat(inject)
                .concat(this._intervals.slice(
                    last_deleted_index + +needMoveLast
                ));

        }

        return this;

    }

    /**
     * @param {Interval} interval
     * @return {Interval}
     */
    concat(interval) {

        for (let i = 0; i < interval._intervals.length; i += 2)
            this.join(interval._intervals.slice(i, i + 1));

        return this

    }

    /**
     * @return {Array.<Number>}
     */
    toArray() {
        return this._intervals;
    }

    /**
     * @return {string}
     */
    toJSON() {
        return "[" + this.pieces.map(a => JSON.stringify(a.toArray())).join(",") + "]"
    }


    /**
     * @return {Number}
     */
    get start() {
        return this._intervals[0]
    }

    /**
     * @return {Number}
     */
    get end() {
        return this._intervals[this._intervals.length - 1]
    }

    /**
     * @return Interval
     */
    get holes() {


        let
            ret = new Interval(0, 1),
            last = this._intervals[this._intervals.length - 1];

        ret._intervals = this._intervals.slice(1, -1);

        if (this._intervals[0] != -Infinity)
            ret._intervals = [-Infinity, this._intervals[0], ...ret._intervals];

        if (last != Infinity)
            ret._intervals = [...ret._intervals, last, Infinity];

        return ret

    }

    /**
     * @return Number
     */
    get length() {

        let s = 0;

        for (let i = 0; i < this._intervals.length; i += 2)
            s += this._intervals[i + 1] - this._intervals[i];

        return s

    }

    /**
     * @return {Array.<Interval>}
     */
    get pieces() {

        let
            inters = this._intervals,
            ret = [];

        for (let i = 0; i < inters.length; i += 2)
            ret.push(new Interval(inters[i], inters[i + 1]))

        return ret

    }


    /**
     * @return {String}
     */
    toString() {
        return "\t"
    }
}

// verqclient:t0g3therW3AreY0ng3
// 5.61.35.218