"use strict";
/**
 * Created by BANO.notIT on 04.01.17.
 */
import {NO_ID} from "./Errors";


export default function (object) {

    if (object === null)
        return Promise.reject(NO_ID);

    else
        return Promise.resolve(object);

}