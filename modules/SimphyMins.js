"use strict";
/**
 * Created by BANO.notIT on 03.02.17.
 */

/**
 * @param {number} inp
 * @return {number}
 */
export default function (inp) {
    const oneMinute = 6e4;
    return (inp / oneMinute | 0) * oneMinute
}