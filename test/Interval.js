"use strict";
/**
 * Created by BANO.notIT on 25.01.17.
 */

let Interval = require("../modules/Interval").default;

require("should");


describe("Interval", function () {

    describe("join", function () {

        it("should add range to end", function () {

            let a = new Interval(0, 1);

            a.join([2, 3]);

            a._intervals.should.be.eql([0, 1, 2, 3]);

            a.join([3, 4]);

            a._intervals.should.be.eql([0, 1, 2, 4]);

        });

        it("should add range to start", function () {

            let a = new Interval(0, 1);

            a.join([-2, -1]);

            a._intervals.should.be.eql([-2, -1, 0, 1]);

            a.join([-3, -2]);

            a._intervals.should.be.eql([-3, -1, 0, 1]);

        });

        it("should remove inner ranges", function () {

            let a = new Interval(0, 3);


            a
                .join([2, 4])
                .join(10, 20);

            a._intervals.should.be.eql([0, 4, 10, 20]);

            a.join(-3, 2);

            a._intervals.should.be.eql([-3, 4, 10, 20]);

            a.join(-3, 20);

            a._intervals.should.be.eql([-3, 20]);

            a.join(10, 25);

            a._intervals.should.be.eql([-3, 25]);

            a
                .join(26, 27)
                .join(28, 29)
                .join(30, 35);

            a._intervals.should.be.eql([-3, 25, 26, 27, 28, 29, 30, 35]);

            a.join(25, 37);

            a._intervals.should.be.eql([-3, 37]);

        })

    })

});

// тут хрень всякая написана, пришлось прям отседова дебажить...