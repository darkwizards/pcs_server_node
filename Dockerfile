FROM node:12

WORKDIR /usr/src/app/

COPY package* /usr/src/app/
RUN npm i

ADD . /usr/src/app/

EXPOSE 80
CMD ["npm", "start"]